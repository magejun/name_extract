package com;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URLDecoder;

public  class GlobalUtils {

    public  static JButton makeButton(String info, ActionListener actionListener){
        JButton jButton = new JButton(info);
        jButton.addActionListener(actionListener);
        return jButton;
    }


    /**
     * 获取jar包编译后的相对路径
     * @return
     */
    public static String acquireJarPath(){
        try{
            String path = GlobalUtils.class.getProtectionDomain().getCodeSource().getLocation().getFile();
            //消除乱码
            path = URLDecoder.decode(path,"UTF-8");
            //根据路径获取目标文件
            File file = new File(path);
            //获取文件的绝对路径
            String jarFilePath = file.getAbsolutePath();
            System.out.println("jarFilePath "+jarFilePath);
            return jarFilePath;
            //获取
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
