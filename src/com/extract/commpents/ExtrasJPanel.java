package com.extract.commpents;

import com.GlobalUtils;
import com.extract.main.ExtractFileName;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * 解析文件名的的组件页
 */
public class ExtrasJPanel extends JPanel {
    private static final String RESOURCES = "/resources/aars";
    private JPanel contentPanel;
    private JPanel controlPanel;
    private static JTextArea jTextArea = new JTextArea();
    private static JTextArea jStatus = new JTextArea();

    public ExtrasJPanel() {
        super(new BorderLayout(), true);
        initExtrasJPanel();
    }

    private void initExtrasJPanel() {
        initContentJPanel();
        initControlJPanel();
        add(contentPanel);
        add(controlPanel,BorderLayout.SOUTH);
    }

    private void initContentJPanel(){
        contentPanel = new JPanel();
        contentPanel.setLayout(new GridLayout(1,2));

        jTextArea = new JTextArea();
        restTextArea();
        jStatus = new JTextArea();
        //        jStatus.setBackground(new Color(123,123,123,123));//这种方式调用，会导致append之后，jStatus里面会出现整个窗口的内容
        jStatus.setBackground(Color.lightGray);
        restStatusTextArea();

        contentPanel.add(jTextArea);
        contentPanel.add(jStatus);
    }

    private void initControlJPanel(){
        controlPanel = new JPanel();
        controlPanel.setSize(800,100);
        controlPanel.add(GlobalUtils.makeButton("提取", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                appendText("开始提取~!\n",Color.GREEN);
                restTextArea();
                if(extractFile()){
                    appendText("提取成功~！\n",Color.GREEN);
                }else{
                    appendText("提取失败~！\n",Color.RED);
                }

            }
        }));
        controlPanel.add(GlobalUtils.makeButton("复制", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StringSelection stsel = new StringSelection(jTextArea.getText());
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stsel, stsel);
                jStatus.append("复制成功，已粘贴到剪切板中！\n");
            }
        }));
        controlPanel.add(GlobalUtils.makeButton("清空列表", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                restTextArea();
                jStatus.append("清空列表~\n");
            }
        }));
        controlPanel.add(GlobalUtils.makeButton("清除资源文件", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                delteResources(RESOURCES);
                jStatus.append("资源文件删除！\n");
            }
        }));
    }


    private static void restTextArea() {
        jTextArea.setText("新版本SDK，版本号：\n");
    }
    private static void restStatusTextArea() {
        jStatus.setText("执行操作：\n");
    }
    private static void appendText(String text,Color color){
        SimpleAttributeSet attrSet = new  SimpleAttributeSet();
        StyleConstants.setForeground(attrSet,color);
        Document document = jStatus.getDocument();
        try {
            document.insertString(document.getLength(),text,attrSet);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
    }

    private static boolean extractFile(){
        java.util.List<String> nameList = getAllFileName(RESOURCES,".aar");
        if(nameList==null ||nameList.size()==0){
            appendText("未找到符合条件的文件或者信息！\n", Color.ORANGE);
            return false;
        }
        java.util.List<String> extrasInfoList = new ArrayList<>();
        for(int i = 0;i<nameList.size();i++){
            extrasInfoList.add(extractInfo(nameList.get(i)));
        }
        sortList(extrasInfoList,0,extrasInfoList.size()-1);

        for (String info:extrasInfoList) {
            System.out.println("file name = "+info);
            jTextArea.append(info+"\n");
        }
        return true;
    }

    public static java.util.List<String> getAllFileName(String path, String suffix){
        java.util.List<String> fileNames = new ArrayList<>();
        try {
            //获取当前jar包的位置
            URL runPath =  ExtractFileName.class.getProtectionDomain().getCodeSource().getLocation();
            if(runPath==null){
                System.out.println("url path = null");
                return fileNames;
            }
            String jarPath = runPath.getPath();
            System.out.println("rp = "+jarPath);
            File jarFile = new File(jarPath);
            String jarParent = jarFile.getParent();
            if(jarFile.isDirectory()){
                jarParent = jarFile.getAbsolutePath();
            }
            File file = new File(jarParent+path);
            boolean exists = file.exists();
            System.out.println("exists "+exists+", file Path = "+file.getPath());
            File[] files = file.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    if(name.endsWith(suffix)){
                        return true;
                    }
                    return false;
                }
            });
            if(files!=null){
                for(int i = 0;i<files.length;i++){
                    fileNames.add(files[i].getName());
                }
            }else{
                System.out.println("files is null");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileNames;
    }

    public static String extractInfo(String name){
        if(!"".endsWith(name) && name!=null){
            String[] splits = name.split("_");
            StringBuilder sb = new StringBuilder();
            if(splits!=null && splits.length>0){
                sb.append(splits[1]);
                sb.append(" ");
                sb.append(splits[3].replace("v","").trim());
            }
            return sb.toString();
        }
        return "";
    }

    //使用快速排序算法，按照版本号大小进行排序
    private static void sortList(List<String> nameList, int left, int right){
        if(left<right){
            int l = left;
            int r = right;
            String pov = nameList.get(left);
            while(l<r){
                try {
                    int pov_version = splitStr2Int(pov);

                    while(l<r && pov_version<splitStr2Int(nameList.get(r))){
                        r--;
                    }
                    if(l<r){
                        nameList.set(l,nameList.get(r));
                        l++;
                    }
                    while(l<r && pov_version>splitStr2Int(nameList.get(l))){
                        l++;
                    }
                    if(l<r){
                        nameList.set(r,nameList.get(l));
                        r--;
                    }


                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
            nameList.set(l,pov);
            sortList(nameList,0,l-1);
            sortList(nameList,l+1,right);
        }

    }

    private static int splitStr2Int(String str){
        if(str!=null){
            String[] strs = str.split(" ");
            return Integer.parseInt(strs[1]);
        }
        return -1;
    }

    private static void delteResources(String resoucesDir){
        try{
            //获取当前jar包的位置
            URL runPath =  ExtractFileName.class.getProtectionDomain().getCodeSource().getLocation();
            if(runPath==null){
                System.out.println("url path = null");
                return;
            }
            String jarPath = runPath.getPath();
            System.out.println("delete = "+jarPath);
            File jarFile = new File(jarPath);
            String jarParent = jarFile.getParent();
            if(jarFile.isDirectory()){
                jarParent = jarFile.getAbsolutePath();
            }
            File file = new File(jarParent,resoucesDir);
            if(file.exists()){
                File[] files = file.listFiles();
                for (File fi :
                        files) {
                    System.out.println("delete = "+fi.getName());
                    fi.deleteOnExit();
                }
            }

        }catch (Exception e){

        }
    }

}
