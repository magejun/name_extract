package com.extract.commpents;

import com.GlobalUtils;
import com.extract.repository.ProductDB;
import com.extract.repository.ProductInfo;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProductJPanel extends JPanel {
    private JPanel contentPanel;
    private JPanel controlPanel;
    private ProductAddDialog addSingleDialog;
    private ProductSearchDialog searchDialog;
    private  String[] columnTitile = {"序号","名字"};
    private DefaultTableModel tableModel;//列表数据源更新， JTable使用的MVC模式，可以通过绑定TableModel，来控制数据源更新

    public ProductJPanel(){
        super(new BorderLayout(),true);
        initProductJPanel();
    }

    private void initProductJPanel(){
        initContentPanel();
        initControlPanel();
//        setBackground(new Color(255,123,123,123));
        add(contentPanel);
        add(controlPanel,BorderLayout.SOUTH);

        addSingleDialog = new ProductAddDialog(this);
        searchDialog = new ProductSearchDialog(this);
    }

    private  void initContentPanel() {
        contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());
        contentPanel.add(createProductTablePanel());
        updateTableData();
    }

    private void initControlPanel(){
        controlPanel = new JPanel();
        controlPanel.add(GlobalUtils.makeButton("刷新列表", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateTableData();
            }
        }));
        controlPanel.add(GlobalUtils.makeButton("增加产品", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addSingleDialog.setLocationRelativeTo(ProductJPanel.this);//居中显示弹窗
                addSingleDialog.setVisible(true);
            }
        }));
        controlPanel.add(GlobalUtils.makeButton("查询", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                searchDialog.setLocationRelativeTo(ProductJPanel.this);//居中显示弹窗
                searchDialog.setVisible(true);
            }
        }));
        controlPanel.setSize(800,100);
    }

    private  JScrollPane createProductTablePanel(){
        tableModel = new DefaultTableModel();
        JTable table = new JTable(tableModel);
        table.setFillsViewportHeight(true);
        JScrollPane scrollPane = new JScrollPane(table);//需要通过这种方式创建！需要通过这种方式创建！需要通过这种方式创建！
        return scrollPane;
    }

    private void updateTableData(){
        updateProductList(new Callback() {
            @Override
            public void onResult(Object[][] results) {
                tableModel.setDataVector(results,columnTitile);//更新table列表数据
            }
        });

    }

    private  void updateProductList(Callback callback){

        ProductDB.INSTANCE.queryAll(new Function1<List<ProductInfo>, Unit>() {
            @Override
            public Unit invoke(List<ProductInfo> list) {
                Object[][] results = null;
                if(list!=null && list.size()>0){
                    results = new Object[list.size()][2];
                    for(int i=0;i<list.size();i++){
                        results[i][0] = list.get(i).getNumber();
                        results[i][1] = list.get(i).getName();
                    }
                }else{
                    results = new Object[0][2];
                }
               if(callback!=null){
                   callback.onResult(results);
               }
               return null;
            }
        });
    }

    public  interface Callback{
        void onResult(Object[][] results);
    }

}
