package com.extract.commpents;

import com.GlobalUtils;
import com.extract.repository.ProductDB;
import com.extract.repository.ProductInfo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Dialog弹窗
 *
 * 增加产品
 */
public class ProductAddDialog extends JDialog {
    JTextField name = new JTextField();//name
    JTextField number = new JTextField();//number

    JButton submit;//确定提交
    Component component;

    public ProductAddDialog(Component component){
        this.component = component;
        setTitle("增加产品");
        this.setLayout(null);
        this.setSize(400,250);
        initAddDialogView();
    }

    private void initAddDialogView() {
        JLabel labelName = new JLabel("产品名：");
        JLabel labelNumber = new JLabel("产品编号：");
        labelName.setBounds(10,10,100,25);
        labelNumber.setBounds(10,50,100,25);

        name.setBounds(150,10,200,30);
        number.setBounds(150,50,200,30);

        submit = GlobalUtils.makeButton("确定", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nameStr = name.getText();
                String numberStr = number.getText();
                System.out.println("name = "+nameStr+",numberStr = "+numberStr);
                ProductInfo info = ProductInfo.obtain(nameStr,numberStr);
                ProductDB.INSTANCE.addProduct(info);
                JOptionPane.showMessageDialog(component,"添加成功");
            }
        });
        submit.setBounds(50,170,300,30);

        add(labelName);
        add(labelNumber);
        add(name);
        add(number);
        add(submit);
    }
}
