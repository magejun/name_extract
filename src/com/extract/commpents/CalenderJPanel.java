package com.extract.commpents;

import com.extract.calendar.CalendarUtils;
import com.extract.calendar.CalenderBean;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class CalenderJPanel extends JPanel {

    public CalenderJPanel(){
        super();
        initCalenderJPanel();
    }

    private void initCalenderJPanel() {
        JPanel startTimePanel = new JPanel();
        startTimePanel.setSize(800,100);
        JLabel year_lable = new JLabel(" 年");
        JLabel month_lable = new JLabel(" 月");
        JLabel day_lable = new JLabel(" 日");

        CalenderBean yearBean = CalendarUtils.getYearArr();
        CalenderBean monthBean = CalendarUtils.getMonthArr();
        CalenderBean dayBean = CalendarUtils.getDayArr(yearBean.getCurValue(),monthBean.getCurValue());
        JComboBox<Integer> year = new JComboBox(yearBean.getElements());
        JComboBox<Integer> month = new JComboBox(monthBean.getElements());
        JComboBox<Integer> day = new JComboBox(dayBean.getElements());

        startTimePanel.add(year);
        startTimePanel.add(year_lable);
        startTimePanel.add(month);
        startTimePanel.add(month_lable);
        startTimePanel.add(day);
        startTimePanel.add(day_lable);
        year.setSelectedIndex(yearBean.getCurIndex());
        month.setSelectedIndex(monthBean.getCurIndex());
        day.setSelectedIndex(dayBean.getCurIndex());
        year.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                Integer year = (Integer) e.getItem();
            }
        });
        month.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                Integer month = (Integer) e.getItem();
            }
        });
        day.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                Integer day = (Integer) e.getItem();
            }
        });


        add(startTimePanel);
    }
}
