package com.extract.commpents;

import com.GlobalUtils;
import com.extract.repository.ProductDB;
import com.extract.repository.ProductInfo;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Dialog弹窗
 *
 * 查找产品
 */
public class ProductSearchDialog extends JDialog {
    JTextField name = new JTextField();//name

    JButton submit;//确定提交
    Component component;

    public ProductSearchDialog(Component component){
        this.component = component;
        setTitle("增加产品");
        this.setLayout(null);
        this.setSize(400,250);
        initAddDialogView();
    }

    private void initAddDialogView() {
        JLabel labelName = new JLabel("查找的产品名：");
        labelName.setBounds(10,10,100,25);

        name.setBounds(150,10,200,30);

        submit = GlobalUtils.makeButton("确定", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nameStr = name.getText();
                System.out.println("name = "+nameStr);
                handleNameListStr(nameStr);
            }
        });
        submit.setBounds(50,170,300,30);

        add(labelName);
        add(name);
        add(submit);
    }

    private final String regex = "\\s+|,|，|、";//使用空格、英文逗号，中文逗号，顿号作为分隔符
    private void handleNameListStr(String nameStr){
        if(nameStr!=null && !nameStr.isEmpty()){
            String result = nameStr.trim();
            ArrayList<String> finalList = new ArrayList<String>();
            String[] arr_1 = result.split(regex);
            if(arr_1.length>0){
//                for (int i=0;i<arr_1.length;i++){
//                    finalList.add(arr_1[i]);
//                }
                ProductDB.INSTANCE.queryByName(arr_1, new Function1<List<ProductInfo>, Unit>() {
                    @Override
                    public Unit invoke(List<ProductInfo> productInfos) {
                        System.out.println("queryByName result "+productInfos);
                        String result = parserProductInfoList(productInfos);
                        JOptionPane.showMessageDialog(ProductSearchDialog.this,result);
                        return null;
                    }
                });
            }
        }
    }

    private String parserProductInfoList(List<ProductInfo> list){
        StringBuffer sb = new StringBuffer();
        int size = list.size();
        sb.append(size+"个产品的信息如下：");
        sb.append("\n");
        for (ProductInfo info:list
             ) {
            sb.append(info.getNumber()+" : "+info.getName());
            sb.append("\n");
        }
        return sb.toString();
    }
}
