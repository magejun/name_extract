package com.extract.repository

data class ProductInfo(var name:String,var number:Int){
    constructor( name: String) : this(name,-1)

    companion object{
        @JvmStatic
        fun  obtain( name:String,number:String):ProductInfo{
            val productInfo = ProductInfo(name)
            productInfo.obtion(name,number)
            return productInfo
        }
    }
}
