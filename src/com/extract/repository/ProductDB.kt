package com.extract.repository

import com.extract.repository.db.JDBCUtils
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

/**
 * 产品信息数据库
 */
object ProductDB {

    val sql_create_table_proudct = "create table if not exists productInfo(name varchar(18) not null,number int,primary key(number))"
    val sql_create_table_proudct_admin = "create table if not exists productAdmin(id number,initEd boolean ,time long,primary key(id))"

    private const val root_id = 999
    private const val patternStr_1 = "[0-9]\\d*"

    private const val productList =
        "0 JhSDk-Demo\t1 UnionSDk-Demo\t6有妖气\t20最右\t40热门小说\t15飞读\t29百度视频\t2书旗小说\t8追书神器\t9聚看点\t12熊猫看书\t30漫画人\t3蚂蚁看点\t35塔读文学\t\t46今日影视\t11神漫画\t50车旺大卡\t多酷游戏\t31 柚次元\t7魅族通用\t17趣读趣写\t5方案商\t18大姨妈\t22游戏通用\t23 动漫之家\t10 UC浏览器\t24趣头条\t25儿歌多多\t26互电\t27 APUS\t28双开\t32淘小说\t33傲游\t34喜爱APP\t39文旅看点\t41天天爱清理\t42连阅小说\t43飘花电影\t44小小美食家\t47爱看书\t48新晴天气\t49点众\t51猴子小说\t52影视大全\t53糖豆\t55猎文小说\t56雷兽\t57中华日历\t59步天天\t61即嗨\t63黑白互联\t65正义枪战\t69 QTT\t70漫客栈\t71中华万年历（iOS)\t72可萌记账\t74爱维宝贝\t78 PP体育\t81幸福奶茶店\t82开心网咖\t83龙腾WIFI\t84一笔记账（iOS)\t87漫画岛\t150 yuezuan\t154趣盟\t60红豆小说\t67酷划\t68阅友\t73速看\t75英迈真\t76迷你小世界\t77微米浏览器\t79得间\t80免费趣小说\t85宝宝巴士\t86欢乐拼字\t88奔逸Wifi\t89盲盒大作战\t90白猫汤屋\t91 5G网络小行家\t92手机环卫大师\t93 5GWifi一键连\t94清理超市\t95 Wifi-免费王\t96小蜜蜂\t97快捷清理\t98手机安全骑士\t99柚子手机大管家\t100全速Wi-Fi手机助手\t101焕新清理\t102手机清理小能手\t103 5G网络天使\t104速连专家 \t105 5G极连助手\t106手机清洁工\t107黑猫WiFi \t108龙锦WiFi\t109自动清理大师\t110清道夫\t111天狗\t112田螺\t113暖心天气\t114仓鼠手机清理\t115明净清理\t116无暇清理\t117猫头鹰\t118啄木鸟\t119一新\t120小新\t121皎洁\t122智充电\t123疾速充电\t124乌鸦\t125金扫把\t126海鸥\t127大雁\t128大象\t129步步乐\t130老虎\t131小灵鱼\t132自新\t133如意\t134金果连连看\t135喜马拉雅\t136砖块点点消\t137快乐打方\t138萌萌爱消消\t139好心情天气\t140红叶天气\t141多彩天气\t142樱花\t143向日葵\t144黑龙马\t145大蜜蜂\t146蜂鸟\t147网络护卫队\t148菠萝\t149 八哥\t151采金小镇\t152朝阳天气\t153小红花"


    init {

        try {
            val conn = JDBCUtils.getConnection()
            println("con #1 $conn")
            conn?.let {
                val createStatement =conn.createStatement()
                createStatement.executeUpdate(sql_create_table_proudct)
                createStatement.executeUpdate(sql_create_table_proudct_admin)

                createStatement.close()
                conn.close()
            }

            if(!hasInited()){
                Thread(object :Runnable{
                    override fun run() {
                        initDBProductList()
                    }
                }).start()

            }
        }catch (e:Exception){
            e.printStackTrace()
        }

    }

    private fun hasInited():Boolean{
        try {
            val conn = JDBCUtils.getConnection()
            println("con #2 $conn")
            conn?.let {
                var hasInited = false
                println("hasInited query  $hasInited")
                val queryStatement = conn.createStatement()
                val result = queryStatement.executeQuery("select initEd from productAdmin where id=$root_id")

                result?.let {
                    println("hasInited set  enter $hasInited")
                    if(result.next()){
                        hasInited =  it.getBoolean("initEd")
                    }
                    println("hasInited set  $hasInited")

                }
                println("hasInited close $hasInited")
                result?.close()
                queryStatement?.close()
                conn.close()
                println("initEd $hasInited")
                return hasInited
            }

        }catch (e:Exception){
            e.printStackTrace()
        }
        return false
    }

    private fun initDBProductList(){
        println("initDBProductList")
        val productListStr = match(productList)
        val productList = mutableListOf<ProductInfo>()
        if(productListStr.isNotEmpty()){
//            println("productListStr = $productListStr")
            val arr = productListStr.split("\t")
            for(i in arr.indices){
                val e = arr[i].trim()
                val eArr = e.split("_")
                if(eArr.size==2){
                    val productInfo = ProductInfo.obtain(eArr[1],eArr[0])
//                    println("index $i, str ${arr[i]} e $e ,productInfo $productInfo")
                    productList.add(productInfo)
                }
            }
            productList.sortBy { productInfo -> productInfo.number }//按number进行升序排列
            addProduct(productList)
            updateInitStatus(true)
            println("init list end!")
        }

    }

    private fun match(content: String):String {
        val matcher = Pattern.compile(patternStr_1).matcher(productList)
        if (matcher != null) {
            val sb = StringBuffer()
            while (matcher.find()) {
                val mstr = matcher.group()
                matcher.appendReplacement(sb, mstr + "_")
            }
            matcher.appendTail(sb)//将最后一次匹配之后，所剩的字符串，添加到一个StringBuffer中
            println("result $sb")
            return sb.toString()
        }
        return ""
    }

    private fun updateInitStatus(init:Boolean){
        val conn = JDBCUtils.getConnection()
        val time = System.currentTimeMillis()
        conn?.let{
//          val sqlStr = "insert into productAdmin(id,initEd,time) values($root_id,$init,$time) on conflict(id) do update set initEd=$init,time=$time;"
//            val sqlStr = "insert or ignore into productAdmin(id,initEd,time) values($root_id,$init,$time); update productAdmin set initEd = $init,time = $time where id = $root_id;"
            val sqlStr = "replace into productAdmin values (?,?,?)"
            val updateStatement = conn.prepareStatement(sqlStr)
            updateStatement.setInt(1, root_id)
            updateStatement.setBoolean(2,init)
            updateStatement.setLong(3,time)
            val result =  updateStatement.executeUpdate()
            println("init update result $result")
            updateStatement.close()
            conn.close()
        }
    }

    private val lock = Object()
    fun queryAll(callback:(List<ProductInfo>)->Unit){
        Thread(object :Runnable{
            override fun run() {
                synchronized(lock){
                    val conn = JDBCUtils.getConnection()
                    val dataList = ArrayList<ProductInfo>()
                    conn?.let{
                        val queryStatement = conn.createStatement()
                        val result =  queryStatement.executeQuery("select * from productInfo order by number ASC")//ASC 默认排序方式-从小到大  DESC 降序排列
                        result?.let {

                            while (it.next()){
                                val product = ProductInfo(it.getString("name"),it.getInt("number"))
                                dataList.add(product)
                            }
                            result.close()
                        }
                        queryStatement.close()
                        conn.close()
                    }
                    callback?.invoke(dataList)
                }
            }
        }).start()

    }


    /**
     * joinToString使用说明：
     *
     * separator 除最后一个元素外，separator将位于每个元素之后
     * prefix 结果字符串将以prefix开头
     * postfix 结果字符串将以postfix结尾
     * limit和truncated： 指定包含在结果中元素的数量，超过limit后，其它元素将被truncated参数的单个值替换
     * 如：arr = (1...100).toList()  arr.joinToString(limit=4,truncated = "<...>"
     *   输出结果：1,2,3,4,<...>
     *
     *transform函数  可以自定义元素本身的表达形式。本方法中用的就是这种方式
     *
     *
     */
    fun queryByName(nameList:Array<String>,callback:(List<ProductInfo>)->Unit){
        Thread(object :Runnable{
            override fun run() {
                val conn = JDBCUtils.getConnection()
                val dataList = ArrayList<ProductInfo>()
                conn?.let{
                    val queryStatement = conn.createStatement()
                    println("namelist 2 ${nameList.joinToString { 
                        "'$it'" }}")
                    val result =  queryStatement.executeQuery("select * from productInfo where name in (${nameList.joinToString{"'$it'" }}) order by number ASC")//ASC 默认排序方式-从小到大  DESC 降序排列
                    result?.let {

                        while (it.next()){
                            val product = ProductInfo(it.getString("name"),it.getInt("number"))
                            dataList.add(product)
                        }
                        result.close()
                    }
                    queryStatement.close()
                    conn.close()
                }
                callback?.invoke(dataList)
            }
        }).start()

    }

    fun addProduct(productInfo: ProductInfo){
        println("add product info "+productInfo)
       addProduct(mutableListOf(productInfo))
    }

    fun addProduct(productInfoList:List<ProductInfo>){
        println("add product list ")
        if(!productInfoList.isNullOrEmpty()){
            val conn = JDBCUtils.getConnection()
            conn?.let{
                val prpareStatement = conn.prepareStatement("insert into productInfo values(?,?)")
                for(item in productInfoList){
                    prpareStatement.setString(1,item.name)
                    prpareStatement.setInt(2,item.number)
                   val result =  prpareStatement.executeUpdate()
//                    println("execute result = $result ,name = ${item.name}")
                }
                prpareStatement.close()
                conn.close()
            }
        }

    }

}