package com.extract.repository.db

import java.io.File
import java.sql.Connection
import java.sql.DriverManager

object JDBCUtils {

//    var driver:String? = null
//    var url:String?=null
//    var user:String?=null
//    var password:String?=null
//
//    init {
//        try {
//            val props:Properties = Properties();
//            val dbPropertiesPath:String = "${JDBCUtils.javaClass.getResource("/com.extract.repository/db/mysql_db.properties").path}"
//            val properFileReader: Reader = FileReader(dbPropertiesPath)
//            props.load(properFileReader)
//            driver = props.getProperty("driver")
//            url = props.getProperty("url")
//            user = props.getProperty("user")
//            password = props.getProperty("password")
//
//        }catch (e:Exception){
//            e.printStackTrace()
//        }
//    }

    val driver:String = "org.sqlite.JDBC"
    var sqlite_db:String? = null;
    init {
        try{

//            sqlite_db = JDBCUtils.javaClass.getResource("/com/extract/repository/db/extract.db").path
            val file = File(/*GlobalUtils.acquireJarPath()+*/"extract.db")//解决生成jar包之后，无法连接数据库的问题
            sqlite_db = file.path
        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    fun getConnection():Connection?{
        try {
            //注册驱动
            Class.forName(driver)

            System.out.println("sqlite_db $sqlite_db")
            //或得连接
            val connection = DriverManager.getConnection("jdbc:sqlite:$sqlite_db")
            return connection
        }catch (e:Exception){

        }
        return null
    }


}
