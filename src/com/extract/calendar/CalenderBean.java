package com.extract.calendar;

public class CalenderBean {
    private Integer[] elements;
    private int curIndex;
    private int curValue;

    public CalenderBean(Integer[] arr,int curIndex,int curValue){
        if(arr!=null){
            this.elements = arr;
        }
        this.curIndex = curIndex;
        this.curValue = curValue;
    }

    public Integer[] getElements() {
        return elements;
    }

    public int getCurIndex() {
        return curIndex;
    }

    public int getCurValue() {
        return curValue;
    }
}
