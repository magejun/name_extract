package com.extract.calendar;

import java.time.LocalDateTime;

/**
 * 日期赋值工具类
 */
public class CalendarUtils {
    private static int yearCount = 2;

    public static int getCurrentYear(){
        LocalDateTime date = LocalDateTime.now();
        return date.getYear();
    }

    public static int getCurMonth(){
        LocalDateTime date = LocalDateTime.now();
        return date.getMonth().getValue();
    }

    public static int getCurDay(){
        LocalDateTime date = LocalDateTime.now();
        return date.getDayOfMonth();
    }


    public static CalenderBean getYearArr(){

        Integer[] yearArr = new Integer[yearCount];
        int year = getCurrentYear();
        yearArr[0] = year;
        yearArr[1] = year-1;
        return new CalenderBean(yearArr,0,year);
    }

    public static CalenderBean getMonthArr(){
        Integer[] monthArr = new Integer[12];
        initArr(monthArr,12);
        int curMonth = getCurMonth();
        CalenderBean calenderBean = new CalenderBean(monthArr,curMonth-1,curMonth);
        return calenderBean;
    }

    public static CalenderBean getDayArr(int year,int month){
        int dayCount = getDayCounts(year,month);
        Integer[] dayArr = new Integer[dayCount];
        initArr(dayArr,dayCount);
        int curDay = getCurDay();
        CalenderBean calenderBean = new CalenderBean(dayArr,curDay-1,curDay);
        return calenderBean;
    }

    private static void initArr(Integer[] arr,int size){
        for (int i = 0;i<size;i++){
            arr[i] = i+1;
        }
    }


    public static int getDayCounts(int year,int month){
        int day = 1;
        switch (month){
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                day =  31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                day = 30;
                break;
            case 2:
                //能被4整除，并且不能被100整除的，是普通闰年
                //能被400整除的，是世纪闰年
                if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0){
                    System.out.println(year+"年"+month+"月共有29天");
                    day = 29;
                }else {
                    System.out.println(year + "年" + month + "月共有28天");
                    day = 28;
                }
                break;
        }
        return day;
    }

}
