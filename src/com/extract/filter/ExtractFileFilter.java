package com.extract.filter;

import java.io.File;
import java.io.FileFilter;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;

public class ExtractFileFilter implements FileFilter {

    private String suffix = ".aar";
    private Date startDate,endDate;

    public ExtractFileFilter(String suffix,Date startDate,Date endDate){

    }



    @Override
    public boolean accept(File pathname) {
        if(pathname!=null && pathname.getName().endsWith(suffix) ){
            long fileCreateDate = getFileDate(pathname.getAbsolutePath());
            long start_date = startDate.getTime();
            long end_date = endDate.getTime();
            System.out.println("file create date "+fileCreateDate+", start_date "+start_date+", end_date "+end_date );
            if(fileCreateDate>=start_date && fileCreateDate<=end_date){
                System.out.println("filter file!");
                return true;
            }

        }
        return false;
    }

    /**
     * 获取文件的创建日期
     * @param url
     * @return
     */
    private long getFileDate(String url){
        File file = new File(url);
        try {
            Path path= Paths.get(url);
            BasicFileAttributeView basicView= Files.getFileAttributeView(path, BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS );
            BasicFileAttributes attr = basicView.readAttributes();
            return attr.creationTime().toMillis();
        } catch (Exception e) {
            e.printStackTrace();
            return file.lastModified();
        }
    }
}
