package com.extract.repository

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

inline fun<T,R> T.let(block:(T)->R):R{
//    contract {
//        callsInPlace(block,InvocationKind.EXACTLY_ONCE)
//    }
    return block(this)
}

public fun ProductInfo.obtion(name:String,number:String){
    try{
        val intNumber = number.toInt()
        this.number = intNumber
        this.name = name
    }catch (e:Exception){

    }
}