package com.extract.main;

import com.GlobalUtils;
import com.extract.commpents.CalenderJPanel;
import com.extract.commpents.ExtrasJPanel;
import com.extract.commpents.ProductJPanel;

import javax.swing.*;
import java.awt.*;

public class ExtractFileName {
    public static void main(String[] args) {
        initView();
    }


    private static void initView(){
        JFrame gameFrame = new JFrame("提取文件名和版本号");
        gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);////用户单击窗口的关闭按钮时程序执行的操作
        gameFrame.setLocationRelativeTo(null);//传入null，会让窗口居中显示
        gameFrame.setResizable(true);//设置此窗体是否可由用户调整大小
        gameFrame.setSize(800,500);
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.add("包名提取",new ExtrasJPanel());
        tabbedPane.add("产品列表",new ProductJPanel());
        gameFrame.add(new CalenderJPanel(),BorderLayout.NORTH);//添加日期选择
        gameFrame.add(tabbedPane);//添加主页导航布局

        gameFrame.setVisible(true);//  //设置窗体可见，没有该语句，窗体将不可见，此语句必须有，否则没有界面就没有如何意义了
//        ProductAddDialog  addSingleDialog = new ProductAddDialog(gameFrame);
//        addSingleDialog.setVisible(true);

    }

}
